package com.coki.repository;

import com.coki.domain.SpaceEvent;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the SpaceEvent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SpaceEventRepository extends JpaRepository<SpaceEvent, Long> {}
