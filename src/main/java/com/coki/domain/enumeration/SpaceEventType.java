package com.coki.domain.enumeration;

/**
 * The SpaceEventType enumeration.
 */
public enum SpaceEventType {
    LAUNCH,
    LANDING,
}
